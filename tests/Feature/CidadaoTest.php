<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Cidadao as CidadaoModel;
use App\Http\Resources\Cidadao as CidadaoResource;
use App\Models\CidadaoContato as CidadaoContatoModel;

/**
 * Class CidadaoTest
 *
 * @package Tests\Unit
 */
class CidadaoTest extends TestCase
{
    const URI = '/api/cidadaos';

    /**
     * A successful API test when a listing all citizens.
     *
     * @return void
     */
    public function testApiList()
    {
        $response = $this->get(self::URI);
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'cpf',
                        'nome',
                        'sobrenome',
                        'contatos' => [
                            'id',
                            'cidadao_id',
                            'email',
                            'telefone',
                            'celular',
                        ],
                        'endereco' => [
                            'id',
                            'cidadao_id',
                            'cep',
                            'logradouro',
                            'bairro',
                            'cidade',
                            'uf',
                        ]
                    ],
                ],
            ]);
    }

    /**
     * A failing API test when creating the citizen that requires all fields is empty.
     *
     * @return void
     */
    public function testApiCreateRequiresAllFields()
    {
        $response = $this->json('POST', self::URI, []);
        $response->assertStatus(422)
            ->assertExactJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "cpf" => [
                        0 => "O CPF deve ser informado."
                    ],
                    "cep" => [
                        0 => "O CEP deve ser informado."
                    ],
                    "nome" => [
                        0 => "O nome do cidadão deve ser informado."
                    ],
                    "contatos.email" => [
                        0 => "O email para contato deve ser informado."
                    ],
                    "sobrenome" => [
                        0 => "O sobrenome do cidadão deve ser informado."
                    ]
                ]
            ]);
    }

    /**
     * A failing API test when creating the citizen with invalid cep.
     *
     * @return void
     */
    public function testApiCreateInvalidFields()
    {
        $response = $this->json('POST', self::URI, [
            'nome' => 'Teste',
            'sobrenome' => 'Teste',
            'cpf' => 12345,
            'cep' => 123,
            'contatos' => [
                'email' => 'email'
            ]
        ]);
        $response->assertStatus(422)
            ->assertExactJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "cep" => [
                        0 => 'O CEP deve conter 8 dígitos. Ex.: 12345678.'
                    ],
                    "cpf" => [
                        0 => "O CPF informado é inválido.",
                        1 => "O CPF deve conter 11 dígitos. Ex.: 12345678900."
                    ],
                    "contatos.email" => [
                        0 => "O email informado é inválido."
                    ]
                ]
            ]);
    }

    /**
     * A failing API test when creating the citizen with cpf duplicity.
     *
     * @return void
     */
    public function testApiCreateDuplicityCPF()
    {
        $cidadao = CidadaoResource::make(factory(CidadaoModel::class)->create());

        $response = $this->json('POST', self::URI, [
            'nome' => 'Teste',
            'sobrenome' => 'Teste',
            'cpf' => $cidadao->cpf,
            'cep' => 83800972,
            'contatos' => [
                'email' => 'email@teste.com'
            ]
        ]);
        $response->assertStatus(422)
            ->assertExactJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "cpf" => [
                        0 => "Não é possível cadastrar o CPF informado, pois o mesmo já está cadastrado."
                    ]
                ]
            ]);
    }

    /**
     * A successful API test when creating the citizen.
     *
     * @return void
     */
    public function testApiCreateSuccess()
    {
        $contato = factory(CidadaoContatoModel::class)->make();
        $cidadao = factory(CidadaoModel::class)->make(['cep' => 83800972, 'cpf' => 79864731009]);

        $cidadao->setRelation('contatos', $contato);

        $response = $this->json('POST', self::URI, $cidadao->jsonSerialize());
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'cpf',
                    'nome',
                    'sobrenome',
                    'contatos' => [
                        'id',
                        'cidadao_id',
                        'email',
                        'telefone',
                        'celular',
                    ],
                    'endereco' => [
                        'id',
                        'cidadao_id',
                        'cep',
                        'logradouro',
                        'bairro',
                        'cidade',
                        'uf',
                    ]
                ],
            ]);
    }
}
