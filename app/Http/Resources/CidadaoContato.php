<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CidadaoContato
 *
 * @package App\Http\Resources
 */
class CidadaoContato extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'email' => $this->email,
            'telefone' => $this->telefone,
            'celular' => $this->celular,
        ];
    }
}
