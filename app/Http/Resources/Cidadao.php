<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\CidadaoContato as CidadaoContatoResource;
use App\Http\Resources\CidadaoEndereco as CidadaoEnderecoResource;

/**
 * Class Cidadao
 *
 * @package App\Http\Resources
 */
class Cidadao extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'cpf' => $this->cpf,
            'nome' => $this->nome,
            'sobrenome' => $this->sobrenome,
            'contatos' => $this->whenLoaded('cidadaoContato'),
            'endereco' => $this->whenLoaded('cidadaoEndereco'),
        ];
    }
}
