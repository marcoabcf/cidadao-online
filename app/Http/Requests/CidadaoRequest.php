<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CidadaoRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'sobrenome' => 'required',
            'cep' => 'digits:8|required',
            'contatos.email' => 'email|required',
            'cpf' => "cpf|digits:11|required|unique:cidadao|numeric"
        ];

        if ($this->isMethod('PUT') || $this->isMethod('PATCH')) {
            $id = $this->cidadao;
            $rules['cpf'] = "cpf|digits:11|required|unique:cidadao,id,{$id}|numeric";

            if ($this->isMethod('PATCH')) {
                $rules = $this->getRulesPatch($id);
            }
        }

        return $rules;
    }

    /**
     * Returns a rules for method PATCH.
     *
     * @param $id
     * @return array
     */
    private function getRulesPatch($id)
    {
        $rules = [];
        $fields = $this->toArray();

        if (! empty($fields['nome'])) {
            $rules = array_merge($rules, ['nome' => 'required']);
        }

        if (! empty($fields['sobrenome'])) {
            $rules = array_merge($rules, ['sobrenome' => 'required']);
        }

        if (! empty($fields['cep'])) {
            $rules = array_merge($rules, ['cep' => 'digits:8|required']);
        }

        if (! empty($fields['contatos']) && ! empty($fields['contatos']['email'])) {
            $rules = array_merge($rules, ['contatos.email' => 'email|required']);
        }

        if (! empty($fields['cpf'])) {
            $rules = array_merge($rules, ['cpf' => "cpf|digits:11|required|unique:cidadao,id,{$id}|numeric"]);
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'cpf.cpf' => 'O CPF informado é inválido.',
            'cpf.required' => 'O CPF deve ser informado.',
            'cep.required' => 'O CEP deve ser informado.',
            'contatos.email.email' => 'O email informado é inválido.',
            'contatos.email.required' => 'O email para contato deve ser informado.',
            'nome.required' => 'O nome do cidadão deve ser informado.',
            'cep.digits' => 'O CEP deve conter 8 dígitos. Ex.: 12345678.',
            'cpf.digits' => 'O CPF deve conter 11 dígitos. Ex.: 12345678900.',
            'sobrenome.required' => 'O sobrenome do cidadão deve ser informado.',
            'cpf.unique' => 'Não é possível cadastrar o CPF informado, pois o mesmo já está cadastrado.',
        ];
    }
}
