<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Business\CidadaoBusiness;
use App\Http\Requests\CidadaoRequest;
use App\Http\Resources\Cidadao as CidadaoResource;

/**
 * Class CidadaoController
 *
 * @package App\Http\Controllers\API
 */
class CidadaoController extends Controller
{

    /**
     * CidadaoController constructor.
     *
     * @param CidadaoBusiness $cidadaoBusiness
     */
    public function __construct(CidadaoBusiness $cidadaoBusiness)
    {
        $this->cidadaoBusiness = $cidadaoBusiness;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $cidadaos = $this->cidadaoBusiness->getAll();
        return CidadaoResource::collection($cidadaos);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function filter(Request $request)
    {
        $cidadaos = $this->cidadaoBusiness->getByFilter($request->all());
        return CidadaoResource::collection($cidadaos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CidadaoRequest $request
     * @return CidadaoResource
     * @throws \Exception
     */
    public function store(CidadaoRequest $request)
    {
        $cidadao = $this->cidadaoBusiness->create($request->all());
        return new CidadaoResource($cidadao);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return CidadaoResource
     */
    public function show($id)
    {
        $cidadao = $this->cidadaoBusiness->getById($id);
        return CidadaoResource::make($cidadao);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CidadaoRequest $request
     * @param $id
     * @return CidadaoResource
     * @throws \Exception
     */
    public function update(CidadaoRequest $request, $id)
    {
        $cidadao = $this->cidadaoBusiness->update($request->toArray(), $id);
        return CidadaoResource::make($cidadao);
    }
}
