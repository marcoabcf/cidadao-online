<?php

namespace App\Services;

use FlyingLuscas\ViaCEP\ZipCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;

class EnderecoService
{

    /**
     * Returns a complete address by cep.
     *
     * @param $cep
     * @return array
     */
    public static function getAddressByCep($cep)
    {
        $zipcode = new ZipCode;
        $address = $zipcode->find($cep);

        if (empty($address->zipCode)) {
            throw new HttpResponseException(new JsonResponse([
                "message" => "The given data was invalid.",
                "errors" => [
                    "cep" => "Invalid or non-existent Zipcode."
                ]
            ], 422));
        }

        return static::getFormatedAddress($address);
    }

    /**
     * Returns the address formatted for Brazilian Portuguese.
     *
     * @param $address
     * @return array
     */
    private static function getFormatedAddress($address)
    {
        $addr = [];
        $addr['uf'] = $address->state;
        $addr['cidade'] = $address->city;
        $addr['logradouro'] = $address->street;
        $addr['bairro'] = $address->neighborhood;
        $addr['cep'] = preg_replace("/[^0-9]/", "", $address->zipCode);

        return $addr;
    }
}
