<?php

namespace App\Models;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;


/**
 * Class Cidadao
 *
 * @package App\Models
 */
class Cidadao extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cidadao';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cpf', 'nome', 'sobrenome'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string)Uuid::uuid4();
        });
    }

    /**
     * The relation to CitizenContact and Citizen models.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cidadaoContato()
    {
        return $this->hasOne(CidadaoContato::class, 'cidadao_id', 'id');
    }

    /**
     * The relation to CitizenAddress and Citizen models.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cidadaoEndereco()
    {
        return $this->hasOne(CidadaoEndereco::class, 'cidadao_id', 'id');
    }
}
