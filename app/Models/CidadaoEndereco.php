<?php

namespace App\Models;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CidadaoEndereco
 *
 * @package App\Models
 */
class CidadaoEndereco extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cidadao_endereco';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cidadao_id', 'cep', 'logradouro', 'bairro', 'cidade', 'uf'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string)Uuid::uuid4();
        });
    }

    /**
     * The relation to Citizen and CitizenAddress models.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cidadao()
    {
        return $this->hasOne(Cidadao::class);
    }
}
