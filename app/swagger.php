<?php
/**
 * @SWG\Swagger(
 *     basePath="/api",
 *     @SWG\Info(
 *         title="APIs Service CidadaoOnline",
 *         version="1.0.0",
 *         description="This is a documentation for the CidadaoOnline API service.",
 *     ),
 *
 *     @SWG\Get(
 *         path="/cidadaos",
 *         tags={"cidadao"},
 *         summary="Returns list of Citizen, Contacts and Address.",
 *         operationId="listCitizen",
 *         @SWG\Response(
 *             response=200,
 *             description="Return successful."
 *         ),
 *     ),
 *
 *     @SWG\Get(
 *         path="/cidadaos/{uuid_cidadao}",
 *         tags={"cidadao"},
 *         summary="Get Citizen, Contacts and Address information.",
 *         description="Returns Citizen, Contacts and Address data",
 *         operationId="listSpecificCitizen",
 *         @SWG\Parameter(
 *             name="uuid_cidadao",
 *             description="Citizen UUID",
 *             required=true,
 *             type="string",
 *             format="uuid",
 *             in="path"
 *         ),
 *         @SWG\Response(
 *             response=200,
 *             description="Return successful."
 *         ),
 *     ),
 *
 *     @SWG\Get(
 *         path="/cidadaos/filtrar/?cpf={cpf}",
 *         tags={"cidadao"},
 *         summary="Filter Citizen, Contacts and Address information by CPF number.",
 *         description="Returns Citizen, Contacts and Address data by CPF number.",
 *         operationId="listSpecificCitizenByCPFNumber",
 *         @SWG\Parameter(
 *             name="cpf",
 *             description="Citizen's CPF Number",
 *             required=true,
 *             type="integer",
 *             in="path"
 *         ),
 *         @SWG\Response(
 *             response=200,
 *             description="Return successful."
 *         ),
 *     ),
 *
 *     @SWG\Post(
 *         path="/cidadaos",
 *         tags={"cidadao"},
 *         summary="Add a new citizen to the database.",
 *         operationId="addCitizen",
 *         @SWG\Parameter(
 *             name="cidadao",
 *             description="Citizen's Informations",
 *             required=true,
 *             in="body",
 *             @SWG\Schema(type="string",
 *                 @SWG\Property(
 *                     property="nome",
 *                     type="string",
 *                     example="Honda"
 *                 ),
 *                 @SWG\Property(
 *                     property="sobrenome",
 *                     type="string",
 *                     example="Caiafa Netzel"
 *                 ),
 *                 @SWG\Property(
 *                     property="cpf",
 *                     type="integer",
 *                     example="12345678900"
 *                 ),
 *                 @SWG\Property(
 *                     property="cep",
 *                     type="integer",
 *                     example="12345678"
 *                 ),
 *                 @SWG\Property(
 *                     property="contatos",
 *                     type="object",
 *                     @SWG\Property(
 *                         property="email",
 *                         type="string",
 *                         format="email",
 *                         example="email@email.com"
 *                     ),
 *                     @SWG\Property(
 *                         property="telefone",
 *                         type="integer",
 *                         example="12345678900"
 *                     ),
 *                     @SWG\Property(
 *                         property="celular",
 *                         type="integer",
 *                         example="12345678900"
 *                     )
 *                 )
 *             )
 *         ),
 *         @SWG\Response(
 *             response=200,
 *             description="Create successful."
 *         ),
 *         @SWG\Response(
 *             response=422,
 *             description="The fields are invalid."
 *         )
 *     ),
 *
 *     @SWG\Patch(
 *         path="/cidadaos/{uuid_cidadao}",
 *         tags={"cidadao"},
 *         summary="Change a parcial attributes to citizen.",
 *         operationId="changeParcialCitizen",
 *         @SWG\Parameter(
 *             name="uuid_cidadao",
 *             description="Citizen UUID",
 *             required=true,
 *             type="string",
 *             format="uuid",
 *             in="path"
 *         ),
 *         @SWG\Parameter(
 *             name="cidadao",
 *             description="Citizen's Informations",
 *             required=true,
 *             in="body",
 *             @SWG\Schema(type="string",
 *                 @SWG\Property(
 *                     property="nome",
 *                     type="string",
 *                     example="Honda"
 *                 ),
 *                 @SWG\Property(
 *                     property="sobrenome",
 *                     type="string",
 *                     example="Caiafa Netzel"
 *                 ),
 *                 @SWG\Property(
 *                     property="cpf",
 *                     type="integer",
 *                     example="12345678900"
 *                 ),
 *                 @SWG\Property(
 *                     property="cep",
 *                     type="integer",
 *                     example="12345678"
 *                 ),
 *                 @SWG\Property(
 *                     property="contatos",
 *                     type="object",
 *                     @SWG\Property(
 *                         property="email",
 *                         type="string",
 *                         format="email",
 *                         example="email@email.com"
 *                     ),
 *                     @SWG\Property(
 *                         property="telefone",
 *                         type="integer",
 *                         example="12345678900"
 *                     ),
 *                     @SWG\Property(
 *                         property="celular",
 *                         type="integer",
 *                         example="12345678900"
 *                     )
 *                 )
 *             )
 *         ),
 *         @SWG\Response(
 *             response=200,
 *             description="Change successful."
 *         ),
 *         @SWG\Response(
 *             response=422,
 *             description="The fields are invalid."
 *         )
 *     ),
 *
 *     @SWG\Put(
 *         path="/cidadaos/{uuid_cidadao}",
 *         tags={"cidadao"},
 *         summary="Change all attributes to citizen.",
 *         operationId="changeAllCitizen",
 *         @SWG\Parameter(
 *             name="uuid_cidadao",
 *             description="Citizen UUID",
 *             required=true,
 *             type="string",
 *             format="uuid",
 *             in="path"
 *         ),
 *         @SWG\Parameter(
 *             name="cidadao",
 *             description="Citizen's Informations",
 *             required=true,
 *             in="body",
 *             @SWG\Schema(type="string",
 *                 @SWG\Property(
 *                     property="nome",
 *                     type="string",
 *                     example="Honda"
 *                 ),
 *                 @SWG\Property(
 *                     property="sobrenome",
 *                     type="string",
 *                     example="Caiafa Netzel"
 *                 ),
 *                 @SWG\Property(
 *                     property="cpf",
 *                     type="integer",
 *                     example="12345678900"
 *                 ),
 *                 @SWG\Property(
 *                     property="cep",
 *                     type="integer",
 *                     example="12345678"
 *                 ),
 *                 @SWG\Property(
 *                     property="contatos",
 *                     type="object",
 *                     @SWG\Property(
 *                         property="email",
 *                         type="string",
 *                         format="email",
 *                         example="email@email.com"
 *                     ),
 *                     @SWG\Property(
 *                         property="telefone",
 *                         type="integer",
 *                         example="12345678900"
 *                     ),
 *                     @SWG\Property(
 *                         property="celular",
 *                         type="integer",
 *                         example="12345678900"
 *                     )
 *                 )
 *             )
 *         ),
 *         @SWG\Response(
 *             response=200,
 *             description="Change successful."
 *         ),
 *         @SWG\Response(
 *             response=422,
 *             description="The fields are invalid."
 *         )
 *     ),
 * )
 */
