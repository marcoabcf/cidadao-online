<?php

namespace App\Business;

use App\Services\EnderecoService;
use App\Models\Cidadao as CidadaoModel;
use App\Models\CidadaoContato as CidadaoContatoModel;
use App\Models\CidadaoEndereco as CidadaoEnderecoModel;

/**
 * Class CidadaoBusiness
 *
 * @package App\Business
 */
class CidadaoBusiness extends Business
{

    /**
     * @var CidadaoModel
     */
    private $cidadaoModel;

    /**
     * @var CidadaoContatoModel
     */
    private $cidadaoContatoModel;

    /**
     * @var CidadaoEnderecoModel
     */
    private $cidadaoEnderecoModel;

    /**
     * CidadaoBusiness constructor.
     *
     * @param CidadaoModel $cidadaoModel
     * @param CidadaoContatoModel $cidadaoContatoModel
     * @param CidadaoEnderecoModel $cidadaoEnderecoModel
     */
    public function __construct(
        CidadaoModel $cidadaoModel,
        CidadaoContatoModel $cidadaoContatoModel,
        CidadaoEnderecoModel $cidadaoEnderecoModel
    ) {
        $this->cidadaoModel = $cidadaoModel;
        $this->cidadaoContatoModel = $cidadaoContatoModel;
        $this->cidadaoEnderecoModel = $cidadaoEnderecoModel;
    }

    /**
     * Create a Citizen by params.
     *
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    public function create(array $params)
    {
        try {
            $this->beginTransaction();
            $cidadao = $this->cidadaoModel->create($params);
            $cidadao->cidadaoContato()->create($params['contatos']);

            $endereco = EnderecoService::getAddressByCep($params['cep']);
            $cidadao->cidadaoEndereco()->create($endereco);

            $this->commit();
            return $this->getById($cidadao->id);
        } catch (\Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }

    /**
     * Update a Citizen by params.
     *
     * @param array $params
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update(array $params, $id)
    {
        try {
            $this->beginTransaction();

            $cidadao = $this->cidadaoModel->find($id);
            $cidadao->update($params);

            if (!empty($params['contatos'])) {
                $cidadao->cidadaoContato()->update($params['contatos']);
            }

            if (!empty($params['cep'])) {
                $endereco = EnderecoService::getAddressByCep($params['cep']);
                $cidadao->cidadaoEndereco()->update($endereco);
            }

            $this->commit();
            return $this->getById($cidadao->id);
        } catch (\Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }

    /**
     * Create a Citizen by params.
     *
     * @param array $id
     * @return mixed
     */
    public function getById($id)
    {
        return CidadaoModel::with(['cidadaoContato', 'cidadaoEndereco'])->find($id);
    }

    /**
     * Returns all list a Citizen, Contacts and Address.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return CidadaoModel::with(['cidadaoContato', 'cidadaoEndereco'])->get();
    }

    /**
     * Returns all list a Citizen, Contacts and Address by filter.
     *
     * @param array $filter
     * @return mixed
     */
    public function getByFilter(array $filter)
    {
        return CidadaoModel::with(['cidadaoContato', 'cidadaoEndereco'])->where($filter)->get();
    }
}
