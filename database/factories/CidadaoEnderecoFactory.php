<?php

use Faker\Generator as Faker;

$factory->define(App\Models\CidadaoEndereco::class, function (Faker $faker) {
    return [
        'cep' => $faker->numerify('########'),
        'logradouro' => $faker->streetAddress,
        'bairro' => $faker->streetName,
        'cidade' => $faker->city,
        'uf' => $faker->stateAbbr,
    ];
});
