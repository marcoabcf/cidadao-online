<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCidadaoContatoTable
 */
class CreateCidadaoContatoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cidadao_contato', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('cidadao_id');
            $table->string('email');
            $table->bigInteger('telefone')->length(12)->nullable();
            $table->bigInteger('celular')->length(12)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cidadao_contato');
    }
}
