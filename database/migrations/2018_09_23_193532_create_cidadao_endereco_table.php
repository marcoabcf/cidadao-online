<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCidadaoEnderecoTable
 */
class CreateCidadaoEnderecoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cidadao_endereco', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('cidadao_id');
            $table->integer('cep');
            $table->string('logradouro');
            $table->string('bairro');
            $table->string('cidade');
            $table->char('uf', 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cidadao_endereco');
    }
}
