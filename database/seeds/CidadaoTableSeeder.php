<?php

use Illuminate\Database\Seeder;

/**
 * Class CidadaoTableSeeder
 */
class CidadaoTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Cidadao::class, 20)->create()->each(function ($r) {
            $r->cidadaoContato()->save(factory(App\Models\CidadaoContato::class)->make());
            $r->cidadaoEndereco()->save(factory(App\Models\CidadaoEndereco::class)->make());
        });
    }
}
